import pg from 'pg';
import {setTimeout as setTimeoutPromise} from 'node:timers/promises';
import {readFile} from 'node:fs/promises';
import path from 'node:path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));

const areWeRestoringDb = () => process.env.RESTORE_DB === 'true';
const areWeDumpingDb = () => process.env.DUMP_DB = 'true';
const loadSql = async (fileName) => readFile(path.join(__dirname, fileName), 'utf-8');

/**
 *
 * @returns {Promise<Client>}
 */
async function getPgClient() {
    const client = new pg.Client();
    client.connect();
    return client;
}

async function main() {
    if (areWeDumpingDb() && !areWeRestoringDb()) {
        console.log(`Now we'll copy all the data in the database in a temporary tables!`);
        const pgClient = await getPgClient();
        const rawSql = await loadSql(process.env.DUMP_SQL_FILENAME);
        await pgClient.query(rawSql);
        //TODO Incorrect restore, we should drop tables and create them again
    } else if (areWeRestoringDb()) {
        console.log(`Now we'll delete all the temporary tables and restore the data!`);

        const pgClient = await getPgClient();
        const rawSql = await loadSql(process.env.RESTORE_SQL_FILENAME);
        await pgClient.query(rawSql);

    } else {
        console.error('You should pass envs: RESTORE_DB=true or DUMP_DB=true');
        await setTimeoutPromise(1000);
        process.exit(1);
    }
}

try {
    await main();
    console.log(`Successfully executed!`);
    process.exit(0);
} catch (err) {
    console.error(err);
    await setTimeoutPromise(1000);
    process.exit(1);
}
