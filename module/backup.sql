BEGIN;
CREATE TABLE dupe_request AS (SELECT * FROM request);
CREATE TABLE dupe_nomenclature_user AS (SELECT * FROM nomenclature_user);
CREATE TABLE dupe_replacement_user AS (SELECT * FROM replacement_user);
CREATE TABLE dupe_request_change AS (SELECT * FROM request_change);
CREATE TABLE dupe_request_comment AS (SELECT * FROM request_comment);
CREATE TABLE dupe_request_item AS (SELECT * FROM request_item);
CREATE TABLE dupe_request_reason AS (SELECT * FROM request_reason);
CREATE TABLE dupe_subrequest AS (SELECT * FROM subrequest);
COMMIT;
BEGIN;
TRUNCATE TABLE request CASCADE;
TRUNCATE TABLE replacement_user CASCADE;
TRUNCATE TABLE nomenclature_user CASCADE;
COMMIT;
